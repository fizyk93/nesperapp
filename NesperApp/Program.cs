﻿using System;
using System.Collections.Generic;
using System.IO;
using com.espertech.esper.client;
using com.espertech.esper.client.scopetest;
using com.espertech.esper.compat.collections;
using NesperApp.Utils;

namespace NesperApp
{
    class Program
    {
        static void Main(string[] args)
        {
            InputStream input; 

            try
            {
                input = new InputStream();
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine($"Nie odnaleziono pliku {ex.FileName}");
                return;
            }

            var serviceProvider = EPServiceProviderManager.GetDefaultProvider();

            var administrator = serviceProvider.EPAdministrator;
            var statement =
                administrator.CreateEPL("select Data, Spolka, KursZamkniecia from KursAkcji.win:length(1)");

            var listener = new SupportUpdateListener();
            statement.AddListener(listener);
            statement.Events += (sender, eventArgs) =>
            {
                var time = DateTime.Now.Ticks % 10000000000 / 10000000;
                eventArgs.NewEvents?.ForEach(e => Console.WriteLine($"{time}: ISTREAM : {((IDictionary<string, object>)e.Underlying).ToDebugString()}"));
                eventArgs.OldEvents?.ForEach(e => Console.WriteLine($"{time}: RSTREAM : {((IDictionary<string, object>)e.Underlying).ToDebugString()}"));
            };

            input.Generuj(serviceProvider);
        }
    }
}
