﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class KursAkcji
{
    public string Spolka { get; set; }
    public string Market { get; set; }
    public DateTime Data { get; set; }
    public double KursOtwarcia { get; set; }
    public double WartoscMax { get; set; }
    public double WartoscMin { get; set; }
    public double KursZamkniecia { get; set; }
    public double Obrot { get; set; }

    public KursAkcji() { }

    public KursAkcji(string spolka, string market, DateTime data,
                double kursOtwarcia, double wartoscMax, double wartoscMin,
                double kursZamkniecia, double obrot)
    {
        Spolka = spolka;
        Market = market;
        Data = data;
        KursOtwarcia = kursOtwarcia;
        WartoscMax = wartoscMax;
        WartoscMin = wartoscMin;
        KursZamkniecia = kursZamkniecia;
        Obrot = obrot;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append($"KursAkcji [spolka={Spolka},\tmarket={Market}");
        sb.Append($"data={Data.ToString("yyyy-MM-dd")},\tkursOtwarcie={KursOtwarcia}");
        sb.Append($"wartoscMax={WartoscMax},\twartoscMin={WartoscMin}");
        sb.Append($"kursZamkniecia={KursZamkniecia},\tobrot={Obrot}]");

        return sb.ToString();
    }
}

