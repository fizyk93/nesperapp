﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using com.espertech.esper.client;
using MiscUtil.IO;
using Directory = System.IO.Directory;

namespace NesperApp
{
    public class InputStream
    {
        private readonly DateTime _dataRozpoczecia = new DateTime(2001, 9, 5);
        private readonly DateTime _dataZakonczenia = new DateTime(2001, 9, 20);

        private const int MaxLiczbaBledow = 5;

        public readonly IList<InformacjeOPliku> TablicaInformacjioPlikach;

        public InputStream()
        {
            var rootDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;

            TablicaInformacjioPlikach = new List<InformacjeOPliku>()
            {
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableAPPLE_NASDAQ.csv"), "Apple", "NASDAQ"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableCOCACOLA_NYSE.csv"),"CocaCola", "NYSE"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableDISNEY_NYSE.csv"), "Disney", "NYSE"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableFORD_NYSE.csv"), "Ford", "NYSE"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableGOOGLE_NASDAQ.csv"), "Google", "NASDAQ"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableHONDA_NYSE.csv"), "Honda", "NYSE"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableIBM_NASDAQ.csv"), "IBM", "NASDAQ"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableINTEL_NASDAQ.csv"), "Intel", "NASDAQ"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableMICROSOFT_NASDAQ.csv"), "Microsoft", "NASDAQ"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableORACLE_NASDAQ.csv"), "Oracle", "NASDAQ"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tablePEPSICO_NYSE.csv"), "PepsiCo", "NYSE"),
                new InformacjeOPliku(Path.Combine(rootDir, "Files", "tableYAHOO_NASDAQ.csv"), "Yahoo", "NASDAQ")
            };
        }

        public void Generuj(EPServiceProvider serviceProvider)
        {
            var query = GetKurs()
                .Where(kursAkcji => kursAkcji.Data >= _dataRozpoczecia)
                .TakeWhile(kursAkcji => kursAkcji.Data <= _dataZakonczenia);

            var currentDate = _dataRozpoczecia;
            foreach (var kursAkcji in query)
            {
                if (kursAkcji.Data > currentDate)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    currentDate = kursAkcji.Data;
                }

                //Console.WriteLine(kursAkcji);
                serviceProvider.EPRuntime.SendEvent(kursAkcji);
            }
        }

        private IEnumerable<KursAkcji> GetKurs()
        {
            int errors = 0;
            const string firstLine = "Date,Open,High,Low,Close,Volume,Adj Close";

            KursAkcji kurs;

            while (TablicaInformacjioPlikach.Any())
            {
                try
                {
                    var info = TablicaInformacjioPlikach
                        .OrderBy(p => DateTime.Parse(p.Enumerator.Current.Split(',')[0], CultureInfo.InvariantCulture))
                        .FirstOrDefault();

                    if (info?.Enumerator.MoveNext() != true || info.Enumerator.Current == firstLine)
                    {
                        TablicaInformacjioPlikach.Remove(info);
                        continue;
                    }

                    var split = info.Enumerator.Current.Split(',');

                    kurs = new KursAkcji()
                    {
                        Spolka = info.NazwaSpolki,
                        Market = info.NazwaMarketu,
                        Data = DateTime.Parse(split[0], CultureInfo.InvariantCulture),
                        KursOtwarcia = double.Parse(split[1], CultureInfo.InvariantCulture),
                        WartoscMax = double.Parse(split[2], CultureInfo.InvariantCulture),
                        WartoscMin = double.Parse(split[3], CultureInfo.InvariantCulture),
                        KursZamkniecia = double.Parse(split[4], CultureInfo.InvariantCulture),
                        Obrot = double.Parse(split[5], CultureInfo.InvariantCulture)
                    };
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Błąd parsowania!");

                    if (++errors > MaxLiczbaBledow)
                    {
                        break;
                    }

                    continue;
                }

                yield return kurs;
            }
        }
    }

    public class InformacjeOPliku
    {
        public InformacjeOPliku(string nazwaPliku, string nazwaSpolki, string nazwaMarketu)
        {
            NazwaPliku = nazwaPliku;
            NazwaSpolki = nazwaSpolki;
            NazwaMarketu = nazwaMarketu;

            Enumerator = new ReverseLineReader(nazwaPliku).GetEnumerator();
            Enumerator.MoveNext();
        }

        public string NazwaPliku { get; set; }
        public string NazwaSpolki { get; set; }
        public string NazwaMarketu { get; set; }
        public IEnumerator<string> Enumerator { get; private set; }

    }
}
